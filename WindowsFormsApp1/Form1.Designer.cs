﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.valueXLabel = new System.Windows.Forms.Label();
            this.valueYlabel = new System.Windows.Forms.Label();
            this.valueZLabel = new System.Windows.Forms.Label();
            this.valueXTextBox = new System.Windows.Forms.TextBox();
            this.valueYTextBox = new System.Windows.Forms.TextBox();
            this.valueZTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.valueSLabel = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // valueXLabel
            // 
            this.valueXLabel.AutoSize = true;
            this.valueXLabel.BackColor = System.Drawing.Color.PaleTurquoise;
            this.valueXLabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueXLabel.Location = new System.Drawing.Point(25, 28);
            this.valueXLabel.Margin = new System.Windows.Forms.Padding(16, 0, 0, 0);
            this.valueXLabel.Name = "valueXLabel";
            this.valueXLabel.Size = new System.Drawing.Size(35, 22);
            this.valueXLabel.TabIndex = 0;
            this.valueXLabel.Text = "x =";
            // 
            // valueYlabel
            // 
            this.valueYlabel.AutoSize = true;
            this.valueYlabel.BackColor = System.Drawing.Color.PaleTurquoise;
            this.valueYlabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueYlabel.Location = new System.Drawing.Point(25, 73);
            this.valueYlabel.Margin = new System.Windows.Forms.Padding(16, 0, 0, 0);
            this.valueYlabel.Name = "valueYlabel";
            this.valueYlabel.Size = new System.Drawing.Size(35, 22);
            this.valueYlabel.TabIndex = 1;
            this.valueYlabel.Text = "y =";
            // 
            // valueZLabel
            // 
            this.valueZLabel.AutoSize = true;
            this.valueZLabel.BackColor = System.Drawing.Color.PaleTurquoise;
            this.valueZLabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueZLabel.Location = new System.Drawing.Point(25, 118);
            this.valueZLabel.Margin = new System.Windows.Forms.Padding(16, 0, 0, 0);
            this.valueZLabel.Name = "valueZLabel";
            this.valueZLabel.Size = new System.Drawing.Size(35, 22);
            this.valueZLabel.TabIndex = 2;
            this.valueZLabel.Text = "z =";
            // 
            // valueXTextBox
            // 
            this.valueXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.valueXTextBox.Location = new System.Drawing.Point(76, 25);
            this.valueXTextBox.Margin = new System.Windows.Forms.Padding(16, 16, 16, 0);
            this.valueXTextBox.Name = "valueXTextBox";
            this.valueXTextBox.Size = new System.Drawing.Size(248, 29);
            this.valueXTextBox.TabIndex = 3;
            // 
            // valueYTextBox
            // 
            this.valueYTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.valueYTextBox.Location = new System.Drawing.Point(76, 70);
            this.valueYTextBox.Margin = new System.Windows.Forms.Padding(16, 16, 16, 0);
            this.valueYTextBox.Name = "valueYTextBox";
            this.valueYTextBox.Size = new System.Drawing.Size(248, 29);
            this.valueYTextBox.TabIndex = 4;
            // 
            // valueZTextBox
            // 
            this.valueZTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.valueZTextBox.Location = new System.Drawing.Point(76, 115);
            this.valueZTextBox.Margin = new System.Windows.Forms.Padding(16, 16, 16, 0);
            this.valueZTextBox.Name = "valueZTextBox";
            this.valueZTextBox.Size = new System.Drawing.Size(248, 29);
            this.valueZTextBox.TabIndex = 5;
            // 
            // calculateButton
            // 
            this.calculateButton.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.calculateButton.Location = new System.Drawing.Point(76, 176);
            this.calculateButton.Margin = new System.Windows.Forms.Padding(32, 32, 32, 0);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(129, 31);
            this.calculateButton.TabIndex = 6;
            this.calculateButton.Text = "Обчислити";
            this.calculateButton.UseVisualStyleBackColor = false;
            this.calculateButton.Click += new System.EventHandler(this.calculateButtonClick);
            // 
            // valueSLabel
            // 
            this.valueSLabel.AutoSize = true;
            this.valueSLabel.BackColor = System.Drawing.Color.PaleTurquoise;
            this.valueSLabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueSLabel.Location = new System.Drawing.Point(25, 234);
            this.valueSLabel.Margin = new System.Windows.Forms.Padding(16, 0, 0, 0);
            this.valueSLabel.Name = "valueSLabel";
            this.valueSLabel.Size = new System.Drawing.Size(36, 22);
            this.valueSLabel.TabIndex = 7;
            this.valueSLabel.Text = "s =";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultTextBox.Location = new System.Drawing.Point(77, 231);
            this.resultTextBox.Margin = new System.Windows.Forms.Padding(16, 24, 16, 0);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ReadOnly = true;
            this.resultTextBox.Size = new System.Drawing.Size(247, 29);
            this.resultTextBox.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(102, 314);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 22);
            this.label1.TabIndex = 9;
            this.label1.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(349, 345);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.valueSLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.valueZTextBox);
            this.Controls.Add(this.valueYTextBox);
            this.Controls.Add(this.valueZLabel);
            this.Controls.Add(this.valueYlabel);
            this.Controls.Add(this.valueXLabel);
            this.Controls.Add(this.valueXTextBox);
            this.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 580);
            this.MinimumSize = new System.Drawing.Size(274, 374);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота No1. Завдання 1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label valueXLabel;
        private System.Windows.Forms.Label valueYlabel;
        private System.Windows.Forms.Label valueZLabel;
        private System.Windows.Forms.TextBox valueXTextBox;
        private System.Windows.Forms.TextBox valueYTextBox;
        private System.Windows.Forms.TextBox valueZTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label valueSLabel;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.Label label1;
    }
}

