﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1 {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
        }

        private void calculateButtonClick(object sender, EventArgs e) {
            double x = inputValue("x", valueXTextBox);
            if (double.IsNaN(x)) return;
            double y = inputValue("y", valueYTextBox);
            if (double.IsNaN(y)) return;
            double z = inputValue("z", valueZTextBox);
            if (double.IsNaN(z)) return;

            double firstTermsF = (2 * Math.Cos(Math.Pow(x, 2.0)) - (1.0 / 2.0)) / ((1.0 / 2.0) + Math.Sin(Math.Pow(y, (2 - z))));
            double secondTermsF = Math.Pow(z, 2.0) / (7 - (z / 3.0));

            resultTextBox.Text = $"{(firstTermsF + secondTermsF):F3}";
        }

        private double inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return double.NaN;
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                return double.NaN;
            }
        }
    }
}
