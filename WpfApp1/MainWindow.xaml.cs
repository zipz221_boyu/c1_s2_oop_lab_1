﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            double x = inputValue("x", valueXTextBox);
            if (double.IsNaN(x)) return;
            double y = inputValue("y", valueYTextBox);
            if (double.IsNaN(y)) return;
            double z = inputValue("z", valueZTextBox);
            if (double.IsNaN(z)) return;

            double firstTermsF = (2 * Math.Cos(Math.Pow(x, 2.0)) - (1.0 / 2.0)) / ((1.0 / 2.0) + Math.Sin(Math.Pow(y, (2 - z))));
            double secondTermsF = Math.Pow(z, 2.0) / (7 - (z / 3.0));

            resultTextBox.Text = $"{(firstTermsF + secondTermsF):F3}";
        }

        private double inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return double.NaN;
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return double.NaN;
            }
        }
    }
}
