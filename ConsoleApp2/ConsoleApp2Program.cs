﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2 {
    internal class ConsoleApp2Program {
        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;

            double a = inputValue("a");
            Console.WriteLine();
            double b = inputValue("b");
            Console.WriteLine();
            double c = inputValue("c");

            double discr = Math.Pow(b, 2.0) - (4.0 * a * c);
            Console.WriteLine($"\nДискримінант: {discr:F3}");

            double rootDiscr = Math.Sqrt(discr);

            Console.WriteLine(
                discr < 0 ? "Рівняння немає дійсних коренів"
                : discr == 0 ? $"Рівняння має два рівних корені: {rootQuadraticEquation(a, b, 0):F3}"
                : $"Рівняння має два корені: {rootQuadraticEquation(a, b, rootDiscr):F3}; {rootQuadraticEquation(a, b, -rootDiscr):F3}"
            );

            Console.ReadLine();
        }

        static double inputValue(string valueName) {
            Console.WriteLine($"Введіть значення для \"{valueName}\" та натиснить клавішу Enter");
            Console.Write($"{valueName} = ");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine($"Ви не ввели значення для \"{valueName}\". Будь ласка, введіть значеня!!!\n");
                return inputValue(valueName);
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть введення значення!!!\n"
                );
                return inputValue(valueName);
            }

        }

        static double rootQuadraticEquation(double a, double b, double rootDiscriminant) {
            return (-b + rootDiscriminant) / (2 * a);
        }
    }
}
