﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    internal class ConsoleApp3Program {
        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("\"N\" має бути цілим числом і більшим за 0");

            int n = inputValue();
            double sum = 0;

            for (int i = 0; i < n; i++) sum += Math.Pow((i + 1), (n - i));

            Console.WriteLine($"Сумма: {sum:F0}");

            Console.ReadLine();
        }

        static int inputValue() {
            Console.WriteLine("Введіть значення для \"N\" та натиснить клавішу Enter");
            Console.Write("N = ");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine("Ви не ввели значення для \"N\". Будь ласка, введіть значеня!!!\n");
                return inputValue();
            }

            try {
                int n = int.Parse(value);
                if (n < 1) {
                    Console.WriteLine("Значення \"N\" має бути більше за 0. Будь ласка, введіть значення ще раз!!!\n");
                    return inputValue();
                }
                return n;
            } catch (System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? "Помилка формату введення занчення \"N\"" : "Сталася невідома помилка") +
                        ". Будь ласка, введіть значення ще раз!!!\n"
                );
                return inputValue();
            }

        }
    }
}
