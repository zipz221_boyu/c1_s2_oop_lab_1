﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;

        }

        private void calculateButtonClick(object sender, RoutedEventArgs e) {
            double a = inputValue("a", valueATextBox);
            if (double.IsNaN(a)) return;
            double b = inputValue("b", valueBTextBox);
            if (double.IsNaN(b)) return;
            double c = inputValue("c", valueCTextBox);
            if (double.IsNaN(c)) return;

            double discr = Math.Pow(b, 2.0) - (4.0 * a * c);
            discrValueLabel.Text = $"{discr:F3}";
            discrLabel.Visibility = Visibility.Visible;
            discrValueLabel.Visibility = Visibility.Visible;

            double rootDiscr = Math.Sqrt(discr);
            resultLabel.Text = discr < 0 ? "Рівняння немає дійсних коренів" : discr == 0 ? "Рівняння має два рівних корені" : "Рівняння має два корені";
            resultLabel.Visibility = Visibility.Visible;

            rootFirstTextBox.Text = discr >= 0 ? $"{rootQuadraticEquation(a, b, rootDiscr):F3}" : "";
            rootSecondTextBox.Text = discr > 0 ? $"{rootQuadraticEquation(a, b, -rootDiscr):F3}" : "";
            rootFirstTextBox.Visibility = discr >= 0 ? Visibility.Visible : Visibility.Hidden;
            rootSecondTextBox.Visibility = discr > 0 ? Visibility.Visible : Visibility.Hidden;
        }

        private double inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return double.NaN;
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return double.NaN;
            }
        }

        static double rootQuadraticEquation(double a, double b, double rootDiscriminant) {
            return (-b + rootDiscriminant) / (2 * a);
        }
    }
}
