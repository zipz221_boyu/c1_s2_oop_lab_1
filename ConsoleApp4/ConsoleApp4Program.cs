﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4 {
    internal class ConsoleApp4Program {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Значення \"N\" і \"M\"  мають бути цілими числами та більшими за 0");

            int n = inputValue("N");
            int k = inputValue("K");
            double sum = 0;

            for (int i = 1; i <= n; i++) sum += Math.Pow(i, k);

            Console.WriteLine($"Сумма: {sum:F0}");

            Console.ReadLine();
        }

        static int inputValue(string valueName) {
            Console.WriteLine($"Введіть значення для \"{valueName}\" та натиснить клавішу Enter");
            Console.Write($"{valueName} = ");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine($"Ви не ввели значення для \"{valueName}\". Будь ласка, введіть значеня!!!\n");
                return inputValue(valueName);
            }

            try {
                int n = int.Parse(value);
                if (n < 1) {
                    Console.WriteLine($"Значення \"{valueName}\" має бути більше за 0. Будь ласка, введіть значення ще раз!!!\n");
                    return inputValue(valueName);
                }
                return n;
            } catch (System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка") +
                        ". Будь ласка, введіть значення ще раз!!!\n"
                );
                return inputValue(valueName);
            }

        }
    }
}
