﻿namespace WindowsFormsApp2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.aLabel = new System.Windows.Forms.Label();
            this.bLabel = new System.Windows.Forms.Label();
            this.cLabel = new System.Windows.Forms.Label();
            this.valueATextBox = new System.Windows.Forms.TextBox();
            this.valueBTextBox = new System.Windows.Forms.TextBox();
            this.valueCTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.discrLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.rootFirstTextBox = new System.Windows.Forms.TextBox();
            this.rootSecondTextBox = new System.Windows.Forms.TextBox();
            this.resultTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.discrValueLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.resultTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // aLabel
            // 
            this.aLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aLabel.AutoSize = true;
            this.aLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.aLabel.Location = new System.Drawing.Point(21, 28);
            this.aLabel.Margin = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.aLabel.Name = "aLabel";
            this.aLabel.Size = new System.Drawing.Size(39, 27);
            this.aLabel.TabIndex = 0;
            this.aLabel.Text = "a =";
            this.aLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bLabel
            // 
            this.bLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bLabel.AutoSize = true;
            this.bLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bLabel.Location = new System.Drawing.Point(21, 78);
            this.bLabel.Margin = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.bLabel.Name = "bLabel";
            this.bLabel.Size = new System.Drawing.Size(40, 27);
            this.bLabel.TabIndex = 1;
            this.bLabel.Text = "b =";
            this.bLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cLabel
            // 
            this.cLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cLabel.AutoSize = true;
            this.cLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cLabel.Location = new System.Drawing.Point(21, 128);
            this.cLabel.Margin = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.cLabel.Name = "cLabel";
            this.cLabel.Size = new System.Drawing.Size(37, 27);
            this.cLabel.TabIndex = 2;
            this.cLabel.Text = "c =";
            this.cLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueATextBox
            // 
            this.valueATextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.valueATextBox.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueATextBox.Location = new System.Drawing.Point(72, 25);
            this.valueATextBox.Margin = new System.Windows.Forms.Padding(12, 16, 16, 0);
            this.valueATextBox.MinimumSize = new System.Drawing.Size(80, 4);
            this.valueATextBox.Name = "valueATextBox";
            this.valueATextBox.Size = new System.Drawing.Size(342, 34);
            this.valueATextBox.TabIndex = 3;
            // 
            // valueBTextBox
            // 
            this.valueBTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.valueBTextBox.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueBTextBox.Location = new System.Drawing.Point(72, 75);
            this.valueBTextBox.Margin = new System.Windows.Forms.Padding(12, 16, 16, 0);
            this.valueBTextBox.MinimumSize = new System.Drawing.Size(80, 4);
            this.valueBTextBox.Name = "valueBTextBox";
            this.valueBTextBox.Size = new System.Drawing.Size(342, 34);
            this.valueBTextBox.TabIndex = 4;
            // 
            // valueCTextBox
            // 
            this.valueCTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.valueCTextBox.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.valueCTextBox.Location = new System.Drawing.Point(72, 125);
            this.valueCTextBox.Margin = new System.Windows.Forms.Padding(12, 16, 16, 0);
            this.valueCTextBox.MinimumSize = new System.Drawing.Size(80, 4);
            this.valueCTextBox.Name = "valueCTextBox";
            this.valueCTextBox.Size = new System.Drawing.Size(342, 34);
            this.valueCTextBox.TabIndex = 5;
            // 
            // calculateButton
            // 
            this.calculateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.calculateButton.AutoSize = true;
            this.calculateButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.calculateButton.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateButton.Location = new System.Drawing.Point(270, 191);
            this.calculateButton.Margin = new System.Windows.Forms.Padding(32, 32, 16, 0);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Padding = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.calculateButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.calculateButton.Size = new System.Drawing.Size(144, 48);
            this.calculateButton.TabIndex = 6;
            this.calculateButton.Text = "Обчислити";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButtonClick);
            // 
            // discrLabel
            // 
            this.discrLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.discrLabel.AutoSize = true;
            this.discrLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.discrLabel.Location = new System.Drawing.Point(21, 271);
            this.discrLabel.Margin = new System.Windows.Forms.Padding(12, 32, 0, 0);
            this.discrLabel.Name = "discrLabel";
            this.discrLabel.Size = new System.Drawing.Size(161, 27);
            this.discrLabel.TabIndex = 7;
            this.discrLabel.Text = "Дискримінант =";
            this.discrLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.discrLabel.Visible = false;
            // 
            // resultLabel
            // 
            this.resultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultLabel.ForeColor = System.Drawing.Color.LimeGreen;
            this.resultLabel.Location = new System.Drawing.Point(21, 314);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(12, 16, 12, 0);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(393, 27);
            this.resultLabel.TabIndex = 8;
            this.resultLabel.Text = "Рівняння має два корені";
            this.resultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.resultLabel.Visible = false;
            // 
            // rootFirstTextBox
            // 
            this.rootFirstTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rootFirstTextBox.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rootFirstTextBox.ForeColor = System.Drawing.SystemColors.Highlight;
            this.rootFirstTextBox.Location = new System.Drawing.Point(0, 0);
            this.rootFirstTextBox.Margin = new System.Windows.Forms.Padding(0, 0, 8, 0);
            this.rootFirstTextBox.MinimumSize = new System.Drawing.Size(80, 4);
            this.rootFirstTextBox.Name = "rootFirstTextBox";
            this.rootFirstTextBox.ReadOnly = true;
            this.rootFirstTextBox.Size = new System.Drawing.Size(186, 34);
            this.rootFirstTextBox.TabIndex = 9;
            this.rootFirstTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rootFirstTextBox.Visible = false;
            // 
            // rootSecondTextBox
            // 
            this.rootSecondTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rootSecondTextBox.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rootSecondTextBox.ForeColor = System.Drawing.SystemColors.Highlight;
            this.rootSecondTextBox.Location = new System.Drawing.Point(202, 0);
            this.rootSecondTextBox.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.rootSecondTextBox.MinimumSize = new System.Drawing.Size(80, 4);
            this.rootSecondTextBox.Name = "rootSecondTextBox";
            this.rootSecondTextBox.ReadOnly = true;
            this.rootSecondTextBox.Size = new System.Drawing.Size(187, 34);
            this.rootSecondTextBox.TabIndex = 10;
            this.rootSecondTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rootSecondTextBox.Visible = false;
            // 
            // resultTableLayoutPanel
            // 
            this.resultTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultTableLayoutPanel.AutoSize = true;
            this.resultTableLayoutPanel.ColumnCount = 2;
            this.resultTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultTableLayoutPanel.Controls.Add(this.rootFirstTextBox, 0, 0);
            this.resultTableLayoutPanel.Controls.Add(this.rootSecondTextBox, 1, 0);
            this.resultTableLayoutPanel.Location = new System.Drawing.Point(25, 357);
            this.resultTableLayoutPanel.Margin = new System.Windows.Forms.Padding(16);
            this.resultTableLayoutPanel.Name = "resultTableLayoutPanel";
            this.resultTableLayoutPanel.RowCount = 1;
            this.resultTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultTableLayoutPanel.Size = new System.Drawing.Size(389, 34);
            this.resultTableLayoutPanel.TabIndex = 11;
            // 
            // discrValueLabel
            // 
            this.discrValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.discrValueLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.discrValueLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.discrValueLabel.Location = new System.Drawing.Point(182, 271);
            this.discrValueLabel.Margin = new System.Windows.Forms.Padding(0, 32, 12, 0);
            this.discrValueLabel.Name = "discrValueLabel";
            this.discrValueLabel.Size = new System.Drawing.Size(236, 27);
            this.discrValueLabel.TabIndex = 12;
            this.discrValueLabel.Text = "0";
            this.discrValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.discrValueLabel.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(192, 410);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 22);
            this.label1.TabIndex = 13;
            this.label1.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 441);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.discrValueLabel);
            this.Controls.Add(this.resultTableLayoutPanel);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.discrLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.valueCTextBox);
            this.Controls.Add(this.valueBTextBox);
            this.Controls.Add(this.valueATextBox);
            this.Controls.Add(this.cLabel);
            this.Controls.Add(this.bLabel);
            this.Controls.Add(this.aLabel);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(900, 480);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(320, 455);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота No1. Завдання 2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.resultTableLayoutPanel.ResumeLayout(false);
            this.resultTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label aLabel;
        private System.Windows.Forms.Label bLabel;
        private System.Windows.Forms.Label cLabel;
        private System.Windows.Forms.TextBox valueATextBox;
        private System.Windows.Forms.TextBox valueBTextBox;
        private System.Windows.Forms.TextBox valueCTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label discrLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.TextBox rootFirstTextBox;
        private System.Windows.Forms.TextBox rootSecondTextBox;
        private System.Windows.Forms.TableLayoutPanel resultTableLayoutPanel;
        private System.Windows.Forms.Label discrValueLabel;
        private System.Windows.Forms.Label label1;
    }
}

