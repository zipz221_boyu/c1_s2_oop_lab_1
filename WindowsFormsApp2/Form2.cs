﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e) {
            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
        }

        private void calculateButtonClick(object sender, EventArgs e) {
            double a = inputValue("a", valueATextBox);
            if (double.IsNaN(a)) return;
            double b = inputValue("b", valueBTextBox);
            if (double.IsNaN(b)) return;
            double c = inputValue("c", valueCTextBox);
            if (double.IsNaN(c)) return;

            double discr = Math.Pow(b, 2.0) - (4.0 * a * c);
            discrValueLabel.Text = $"{discr:F3}";
            discrLabel.Visible = true;
            discrValueLabel.Visible = true;

            double rootDiscr = Math.Sqrt(discr);
            resultLabel.Text = discr < 0 ? "Рівняння немає дійсних коренів" : discr == 0 ? "Рівняння має два рівних корені" : "Рівняння має два корені";
            resultLabel.Visible = true;

            rootFirstTextBox.Text = discr >= 0 ? $"{rootQuadraticEquation(a, b, rootDiscr):F3}" : "";
            rootSecondTextBox.Text = discr > 0 ? $"{rootQuadraticEquation(a, b, -rootDiscr):F3}" : "";
            rootFirstTextBox.Visible = discr >= 0;
            rootSecondTextBox.Visible = discr > 0;
        }

        private double inputValue(string valueName, TextBox textBox) {
            string value = textBox.Text;

            if (string.IsNullOrEmpty(value)) {
                MessageBox.Show($"Ви не ввели значення для \"{valueName}\"", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return double.NaN;
            }

            try {
                return double.Parse(value);
            } catch (System.Exception e) {
                MessageBox.Show(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка"),
                    "Помилка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                return double.NaN;
            }
        }

        static double rootQuadraticEquation(double a, double b, double rootDiscriminant) {
            return (-b + rootDiscriminant) / (2 * a);
        }
    }
}
