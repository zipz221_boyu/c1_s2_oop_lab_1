﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1 {
    internal class ConsoleApp1Program {
        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo) System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;

            double x = inputValue("x");
            Console.WriteLine();
            double y = inputValue("y");
            Console.WriteLine();
            double z = inputValue("z");

            double firstTermsF = (2 * Math.Cos(Math.Pow(x, 2.0)) - (1.0 / 2.0)) / ((1.0 / 2.0) + Math.Sin(Math.Pow(y, (2 - z))));
            double secondTermsF = Math.Pow(z, 2.0) / (7 - (z / 3.0));

            Console.WriteLine($"\nРезультат обчислення: s = {(firstTermsF + secondTermsF):F3}");

            Console.ReadLine();
        }

        static double inputValue(string valueName) {
            Console.WriteLine($"Введіть значення для \"{valueName}\" та натиснить клавішу Enter");
            Console.Write($"{valueName} = ");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine($"Ви не ввели значення для \"{valueName}\". Будь ласка, введіть значеня!!!\n");
                return inputValue(valueName);
            }

            try {
                return double.Parse(value);
            } catch(System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? $"Помилка формату введення занчення \"{valueName}\"" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть введення значення!!!\n"
                );
                return inputValue(valueName);
            }

        }
    }
}
