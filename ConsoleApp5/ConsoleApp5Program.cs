﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5 {
    internal class ConsoleApp5Program {
        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            int value = 0;
            int numberEven = 0;
            int numberOdd = 0;
            int numberPositive = 0;
            int numberNegative = 0;

            Console.WriteLine("Введіть список цілих чисел. Введіть 0, щоб завершити введення!\n");

            do {
                value = inputValue();

                if (value == 0) continue;
                else if (value > 0) numberPositive++;
                else numberNegative++;

                if ((value % 2) == 0) numberEven++; else numberOdd++;
            } while (value != 0);

            Console.WriteLine();
            Console.WriteLine($"Кількість парних чисел: {numberEven}");
            Console.WriteLine($"Кількість непарних чисел: {numberOdd}");
            Console.WriteLine($"Кількість додатніх чисел: {numberPositive}");
            Console.WriteLine($"Кількість від’ємних чисел: {numberNegative}");

            Console.ReadLine();
        }

        static int inputValue() {
            Console.WriteLine("Введіть значення та натиснить клавішу Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine("Ви не ввели значення. Будь ласка, введіть значеня!!!\n");
                return inputValue();
            }

            try {
                int n = int.Parse(value);
                return n;
            } catch (System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, введіть значення ще раз!!!\n"
                );
                return inputValue();
            }

        }
    }
}
